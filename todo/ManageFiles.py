# from ToDo import ToDo
# import json
# import jsonpickle
#
# class ManageFile(object):
#     def __init__(self,fileName):
#         self.fileName = fileName
#
#     def getToDos(self):
#         #dataJson = []
#         with open(self.fileName) as dataFile:
#            #if len(dataFile.readline()) > 0:
#             dataJson = json.load(dataFile)
#         toDos=[]
#
#         for json_ in dataJson:
#             message= json_["message"]
#             id= json_["id"]
#             if str(json_["done"])=="True":
#                 done= True
#             else:
#                 done=False
#             created= json_["created"]
#             toDos.append(ToDo(message,id,done,created))
#         return toDos
#
#     def rewriteFile(self, toDos):
#         json_='['
#         json_+=','.join({jsonpickle.encode(todo, unpicklable=False) for todo in toDos})
#         json_+=']'
#         f = open(self.fileName, 'w')
#         f.write(json_)
#         f.close()
#
#     def writeToXml(self,toDos,xmlFileName):
#         strXml='<?xml version="1.0"?> \n' \
#                '<TODOS>\n'
#         for todo in toDos:
#                 strXml+='<todo>\n'
#                 attrsDic= vars(todo)
#                 for key,val in attrsDic.items():
#                     strXml+='<'+str(key)+'>'+str(val)+'</'+str(key)+'>\n'
#                 strXml+='</todo>\n'
#
#         strXml+='</TODOS>'
#         f = open(xmlFileName, 'w')
#         f.write(strXml)
#         f.close()
#
# # manageFile=ManageFile()
# # manageFile.writeToXml([ToDo('aici',1)],'toDo.xml')
