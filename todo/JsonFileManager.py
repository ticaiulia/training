from ToDo import ToDo
import json
import jsonpickle

class JsonFileManager(object):
    def __init__(self,fileName):
        self.fileName = fileName

    def add(self, todo):
        toDosList=self.getToDos()
        toDosList.append(todo)
        self.rewriteFile()

    def doneTask(self, id):
        toDosList=self.getToDos()
        for todo in toDosList:
            if todo.id==id:
                todo.done=True
                self.rewriteFile(toDosList)
                break

    def getUnDonesToDos(self):
        resList=[]
        toDosList=self.getToDos()
        for todo in toDosList:
            resList.append(todo)
        return resList

    def getToDos(self):
        #dataJson = []
        with open(self.fileName) as dataFile:
           #if len(dataFile.readline()) > 0:
            dataJson = json.load(dataFile)
        toDos=[]

        for json_ in dataJson:
            message= json_["message"]
            id= json_["id"]
            if str(json_["done"])=="True":
                done= True
            else:
                done=False
            created= json_["created"]
            toDos.append(ToDo(message,id,done,created))
        return toDos

    def rewriteFile(self, toDos):
        json_='['
        json_+=','.join({jsonpickle.encode(todo, unpicklable=False) for todo in toDos})
        json_+=']'
        f = open(self.fileName, 'w')
        f.write(json_)
        f.close()
