
class ExportToFile(object):
    def writeToXml(self, toDos, xmlFileName):
        strXml = '<?xml version="1.0"?> \n' \
                 '<TODOS>\n'
        for todo in toDos:
            strXml += '<todo>\n'
            attrsDic = vars(todo)
            for key, val in attrsDic.items():
                strXml += '<' + str(key) + '>' + str(val) + '</' + str(key) + '>\n'
            strXml += '</todo>\n'

        strXml += '</TODOS>'
        f = open(xmlFileName, 'w')
        f.write(strXml)
        f.close()