from DBManager import DBManager
from ToDo import ToDo
from JsonFileManager import  JsonFileManager
from ExportToFile import ExportToFile
class Dispatcher(object):
    def __init__(self):
        DBType,path=self.chooseDbType()
        self.DB=DBType(path)
        # self.=ManageFile(fileName)
        self.toDos=self.DB.getToDos()

    def setToDone(self,id):
        '''
        Searches for the toDo with the given id in the toDo list
        :param message:
        throws NameError if not found
        '''

        self.DB.doneTask(id)
        # found = False
        # for todo in self.toDos:
        #     if todo.id == id:
        #         todo.done=True
        #         found = True
        # if not found:
        #     raise NameError("toDo not in the list")

    def addToDo(self,message,created):
        '''
        :param message: toDo's message
        :param id: toDo's id
        :param created: toDo's create date
        :return: 
        '''
        try:
            id = self.get_next_id()
        except IndexError:
            id = 1
        t = ToDo(message, id, False,created)
        self.DB.add(t)

        # self.toDos.append(t)
        # self.manageFile.rewriteFile(self.toDos)

    def getNotDone(self):
        '''
        :return: a list containing the toDo's with done equal to False 
        '''
        rlist = []

        # for todo in self.DB.getUnDonesToDos():
        #     if not todo.done:
        #         rlist.append(todo)
        return self.DB.getUnDonesToDos()
        # return rlist

    def get_next_id(self):
        ids = []
        for t in self.DB.getToDos():
            ids.append(t.id)
        return max(ids)+1

    def writeToXml(self, list_todos, fileName='xmlToDo.xml'):
        exportToFile=ExportToFile()
        exportToFile.writeToXml(list_todos,fileName)

    def chooseDbType(self):

        with open('todo_settings.py') as dataFile:
            db=path=None
            for line in dataFile:
                if 'STORAGE_TYPE' in line:
                    if ('json') in line:
                    # line=line.strip()
                    # line=line.replace('STORAGE_TYPE','')
                    # line=line.replace('=','')
                    # line=line.replace(' ','')
                    # line=line.replace('\n','')
                    # typ=line
                    # print typ
                    # if set(typ) == set('json') :
                        db=JsonFileManager
                    elif 'sqlite' in line:
                        db=DBManager
                if 'STORAGE_PATH' in line:
                    line=line.replace('STORAGE_PATH', '')
                    line=line.replace('=', '')
                    line=line.replace(' ', '')
                    line=line.replace('\n','')
                    line=line.replace('\'','')
                    path=line

                    return db,path