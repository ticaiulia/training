import sys
import Dispatcher
import ManageFiles
import time
import datetime


def help():
    print '''usage: todo.py [-h] {list,add,done} ...

simple todo app

positional arguments:
  {list,add,done}
    list                list
    add                 add
    done                done

optional arguments:
  -h, --help            show this help message and exit'''

def how_to_use(cmd):
    if cmd == 'list':
        print 'give to function parameter list to print all todos'
    elif cmd == 'add':
        print 'give to function 2 parameters first is add and second is the message of todo'
    elif cmd == 'done':
        print 'give to function 2 parameters first is done and second is the id of todo'


if __name__ == '__main__':
    nr_params = len(sys.argv) -1
    # file = ManageFiles.ManageFile()
    # list = file.getToDos()
    db = Dispatcher.Dispatcher()
    # if nr_params < 1 or nr_params > 2:
    #     help()
    # elif nr_params == 1:
    if nr_params>=1:
        cmd = sys.argv[1]
        if cmd == 'list':
            # function that prints todos
            list_todos = db.getNotDone()
            if len(sys.argv)>=3:
                if sys.argv[2]=='--out' and sys.argv[3]=='xml':
                        if len(sys.argv)>=5:
                             fileName = sys.argv[4]
                             db.writeToXml(list_todos, fileName)
                        else:
                            db.writeToXml(list_todos)
            else:
                for todo in list_todos:
                    print todo
        if cmd == '--help':
            help()
    # elif nr_params == 2:
        param1 = sys.argv[1]
        param2 = sys.argv[2]
        if param1 == '-h' and param2 in ['list','add','done']:
            how_to_use(param2)
        elif param1 == 'add':
            #function that add a todo_ param2 is the message
            time = time.time()
            time = datetime.datetime.fromtimestamp(time).strftime('%Y.%m.%d %H:%M:%S')
            print param2
            db.addToDo(param2,time)
            # file.rewriteFile(db.manageFile)
        elif param1 == 'done':
            try:
                param2 = int(param2)
                #function that move the todo_ with id = param2 to done
                db.setToDone(param2)
                file.rewriteFile(db.manageFile)
            except ValueError:
                print 'id must be an integer'






