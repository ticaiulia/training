import sqlite3
import os
# from ToDo import ToDo
from ToDo import ToDo


class DBManager(object):
    def __init__(self,dbFileName):
        self.dbName=dbFileName
        self.createDbTables()

    def createDbTables(self):
        if not os.path.isfile(self.dbName):
            conn = sqlite3.connect(self.dbName)
            tableQuery='CREATE TABLE todos' \
                   '(message text, id int, done bool, created datetime)'
            conn.execute(tableQuery)
            conn.close()


    def add(self,todo):
        conn = sqlite3.connect(self.dbName)
        query = ''' INSERT INTO todos(message,id,done,created)
                     VALUES(?,?,?,?) '''
        cur = conn.cursor()
        print query
        cur.execute(query, [todo.message,todo.id,todo.done,todo.created])
        conn.commit()
        conn.close()


    def doneTask(self,id):
        conn = sqlite3.connect(self.dbName)
        query='UPDATE todos SET done = 1 WHERE  id = ?'
        conn.execute(query,[id])
        conn.commit()
        conn.close()

    def getUnDonesToDos(self):
        conn = sqlite3.connect(self.dbName)
        query='SELECT * from todos where done = 0'
        cur = conn.cursor()
        cur.execute(query)
        resList=cur.fetchall()
        # resList= list(conn.execute(query))
        todos = []
        for todo in resList:
            td=ToDo(str(todo[0]),todo[1],todo[2],str(todo[3]).encode('utf-8'))
            todos.append(td)

        conn.close()
        return todos


    def getToDos(self):
        conn = sqlite3.connect(self.dbName)
        query = 'SELECT * FROM todos'
        resList = list(conn.execute(query))
        todos = []
        for todo in resList:
            td = ToDo(str(todo[0]), todo[1], todo[2], str(todo[3]).encode('utf-8'))
            todos.append(td)

        conn.close()
        return todos

# dbManager=DBManager('todos.db')
# dbManager.add(ToDo('first',2,False))
# dbManager.doneTask(1)
# print dbManager.getTasks()