import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup
import urllib2

def getFromUrl(url):
    urlPage=urllib2.urlopen(url)
    html=BeautifulSoup(urlPage,'xml')
    urlPage.close()
    return html.getText()

def getTopics(xmlURL):
    data = getFromUrl(xmlURL)
    data=str(data.encode('utf8'))
    print data
    tree = ET.fromstring(data)
    root = tree.getroot()
    return root

def printXmlTopics():
    pass

def getXmlTopics():
    pass

print getTopics('https://www.reddit.com/r/pythoncoding/.xml')