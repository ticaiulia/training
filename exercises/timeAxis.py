import turtle

def minMaxScale(times,labels):
    minTime= min(times)
    maxTime = max(times)
    dim= (maxTime- minTime)/9
    retLabels= []
    for time in xrange(minTime, maxTime, dim):
        index=times.index(time)
        retLabels.append(labels[index])

    return retLabels

def drawX(xLeft, list, labels):
    labels=minMaxScale(list,labels)
    t= turtle.Turtle()
    x,y=400-xLeft,300
    t.penup()
    dimX=float((abs(xLeft) + x)/9)
    t.goto(-x,-y+100)
    for value in labels:
        t.write(value)
        x,y = t.pos()
        t.penup()
        t.goto(x+dimX,y)
    t.penup()


def drawY(ySpace, list, labels):
    labels = minMaxScale(list, labels)
    t = turtle.Turtle()
    x, y = 400, 400-ySpace
    t.penup()
    dimY = float((abs(ySpace) + y) / 9)
    t.goto(-x+150, -y)
    for value in labels:
        t.write(value)
        x, y = t.pos()
        t.penup()
        t.goto(x , y+dimY)


def xAxis(xLeft, list,labels):
    drawX(xLeft, list, labels)
    drawY(xLeft, list, labels)

    turtle.done()


#list=[i+20 for i in xrange(20)]
#labels=[chr(i+65)+'re' for i in xrange(20)]
#xAxis(250,list,labels)

