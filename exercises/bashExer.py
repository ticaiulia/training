import os



def getFiles(inputDir):
    files=[]
    for f in os.listdir(inputDir):
        if  os.path.isdir(f)==False and f[-3:]=='.py' :
            files.append(f)
    return files


def findByKeyWord(dirPath, keyWord):
    files = getFiles(dirPath)
    for f in files:
        with open(dirPath+'/'+f) as dataFile:
            readLine = dataFile.readline()
            if keyWord in readLine:
                print f

def findByRegex(dirPath, regex):
    files = getFiles(dirPath)
    for f in files:
        with open(dirPath+'/'+f) as dataFile:
            readLine = dataFile.readline()
            if readLine.search(regex) in readLine:
                print f


findByKeyWord('/home/iulia/PycharmProjects/src', 'class')
findByRegex('/home/iulia/PycharmProjects/src','^class')