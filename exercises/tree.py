import sys
import os

def printFile(dirName,f):
	path = os.path.abspath(f)
	relativePath = path[path.rfind('/'+dirName+'/')+len(dirName)+1:]
	position = relativePath.count('/')
	name=relativePath[relativePath.rfind('/')+1:]
	print '    '*position+ '--| '+ name

def createTree(dirPath):
	dirName= dirPath[dirPath.rfind('/')+1:]
	printFile(dirName,dirName)
	for f in os.listdir(dirPath):
		printFile(dirName,f)
		if os.path.isdir(f):
			createTree(f)
			

def tree():

	createTree(sys.argv[1])

tree()
