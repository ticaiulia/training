import turtle

def rectangle(dimx, dimy, t):
    t.forward(dimx)
    t.right(90)
    t.forward(dimy)
    t.right(90)
    t.forward(dimx)
    t.right(90)
    t.forward(dimy)

def broscuta(dimx,dimy,x,y,t):
    if dimx>1 and dimy>1:
        t.penup()
        t.goto(x,y)
        t.pendown()
        rectangle(dimx, dimy, t)

        x1,y1=t.position()
        t.penup()
        t.goto(x1+10+dimx, y1-dimy)
        t.pendown()
        rectangle(dimx, dimy, t)
        x1,x2=x1+10+dimx, y1-dimy

        x2,y2=t.position()
        t.penup()
        t.goto(x2+dimx, y2-dimy-10)
        t.pendown()
        rectangle(dimx, dimy, t)
        t.left(90)

        dimx = dimx//2-15
        dimy = dimy//2-15
        return broscuta(dimx,dimy,x+10,y-10,t)+broscuta(dimx,dimy,x1+10,y1-10,t) +broscuta(dimx,dimy,x2+10,y2-20,t)
    else:
        return 0

t= turtle.Turtle()
t.speed(10)
broscuta(250,250,-250,250,t)
turtle.done()
