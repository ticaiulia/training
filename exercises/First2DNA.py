from BeautifulSoup import BeautifulSoup
import urllib2

def getFromUrl(url):
    urlPage=urllib2.urlopen(url)
    html=BeautifulSoup(urlPage)
    return html


def printArticle(artTitle, artPage):
    print
    print '\033[1m'+artTitle.getText() + '\033[0m'
    articleText=(artPage.getText()).replace(artTitle.getText(),'')
    for index,word in enumerate(articleText.split()):
        print word,
        if index%10==0:
            print


def getMaxArticle(url,nr,keyWord):
    html = getFromUrl(url)
    allArt= html.findAll('h2', {"class": "article_title"})
    for art in allArt:
        if nr==0:
            break
        art=art.findChild()
        title = art.get('title')

        if title!=None:
            title= title.encode('utf8')
            if keyWord in title:
                href=art['href']
                htmlArt = getFromUrl(href)
                artPage=htmlArt.find('div', {"id": "articleContent"})
                artTitle= artPage.findChild('strong')
                printArticle(artTitle,artPage)
                nr-=1



# getMaxArticle('http://www.hotnews.ro/',2,'DNA')