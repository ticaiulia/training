import sys
from Dispatcher import Dispatcher

def printList(list):
    if len(list)==0:
        print "List it's empty"
        return
    for element in list:
        print str(element[0].encode('utf8'))


def printMenu():
    print 'Commands: \n' \
          '   1. read-[absoluteDBFileName] list-artists --page [pageNumer] --pageSize [pageSize]\n' \
          '         print artists from page [pageNumber] with size [pageSize]\n' \
          '         from db [absoluteDBFileName] \n' \
          '' \
          '   2. read-[absoluteDBFileName] long-tracks [minNr]\n' \
          '         print artists from [absoluteDBFileName] with at least one tracks with \n' \
          '         [minNr] minutes \n' \
          '' \
          '   3. [comm1 | comm2] --output [fileNamePath]\n' \
          '         write in file [fileNamePath] list resulted from comm1 or comm2 \n' \
          '' \


def main():
    printMenu()
    fullCommand= sys.argv[1:]
    if 'read-' in fullCommand[0]:
        index=fullCommand[0].index('-')
        dbFilePath=fullCommand[0][index+1:]
        dispatcher=Dispatcher(dbFilePath)
        comm=fullCommand[1]
        if comm=="list-artists":
            if fullCommand[2]=='--page':
                nrPage=int(fullCommand[3])
                if fullCommand[4]=='--pageSize':
                    pageSize=int(fullCommand[5])
                    resultList=dispatcher.listArtist(nrPage,pageSize)
                    if len(fullCommand)<=6:
                        printList(resultList)
                    else:
                        if fullCommand[6]=='--output':
                            fileNamePath = fullCommand[7]
                            dispatcher.writeListOnCsv(fileNamePath,resultList)
                        else:
                            print "Invalid command"
                else:
                    print "1Invalid command"
            else:
                print "2Invalid command"

        elif comm=="long-tracks":
            nrMin = fullCommand[2]
            resultList = dispatcher.artWithTracksBigger(int(nrMin))
            if len(fullCommand)<=3:
                printList(resultList)
            if fullCommand[3] == '--output':
                fileNamePath = fullCommand[4]
                dispatcher.writeListOnCsv(fileNamePath, resultList)
            else:
                print "Invalid command"
        else:
            print "3Invalid command"

    else:
        print "4Invalide fullCommand"


main()