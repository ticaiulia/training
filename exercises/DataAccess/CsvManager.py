import csv

class CsvManager(object):
    def writeListInCsv(self,fileName,list):
        l=[]
        for index in xrange(len(list)):
            l.append([list[index][0].encode('utf8')])
        with open(fileName, 'w') as f:
            writer = csv.writer(f)
            writer.writerows(l)



# csvManager= CsvManager()
# csvManager.writeListInCsv('aici.csv',[['ana','ioana','crist'],['alin','mada']])
