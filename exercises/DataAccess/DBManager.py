import sqlite3

class DBManager(object):
    def __init__(self,dbFileName):
        self.fileName=dbFileName

    def getFromDb(self,page,pageSize):
        conn = sqlite3.connect(self.fileName)
        first=(page-1)*pageSize
        last= first + pageSize
        query='SELECT artists.name FROM artists WHERE ArtistId> ? and ArtistId<=?'
        resList=list(conn.execute(query,[first,last]))
        conn.close()
        return  resList

    def artWithTracksBigger(self,mins):
        conn = sqlite3.connect(self.fileName)
        query='SELECT DISTINCT artists.name ' \
              ' FROM artists ' \
              '     INNER JOIN albums ON albums.ArtistId = artists.ArtistId ' \
              '     INNER JOIN tracks ON tracks.AlbumId = albums.AlbumId ' \
              'where tracks.Milliseconds > 60000* ?'
        resSet=list(set(conn.execute(query,[mins])))

        conn.close()
        return resSet

# dbManager= DBManager('/home/iulia/Documents/git-exercises/chinook.db')
# print dbManager.getFromDb(4,12)
# print dbManager.artWithTracksBigger(10)


