from DBManager import  DBManager
from CsvManager import CsvManager


class Dispatcher(object):
    def __init__(self,dbFileName):
        self.dbManager=DBManager(dbFileName)
        self.csvManager=CsvManager()
    def listArtist(self,page,pageSize):
        return self.dbManager.getFromDb(page,pageSize)
    def artWithTracksBigger(self,nrMin):
        return self.dbManager.artWithTracksBigger(nrMin)
    def writeListOnCsv(self,fileName,list):
        self.csvManager.writeListInCsv(fileName,list)