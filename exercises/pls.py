import os
import sys
from colored_term import colored_text,toesc


def getDirs(inputDir):
	dirs=  [d for d in os.listdir(inputDir) if os.path.isdir(d)]
	dirs.sort()
	return dirs	


def maxDim(l1):
	return max([len(f) for f in l1])

def getFiles(inputDir):
	files=[]
	dirs = getDirs(inputDir)
	for f in os.listdir(inputDir):
		if f not in dirs:
			files.append(f)
	files.sort()	
	return files

def printDirs(dirs,maxlen):
	for d in dirs:
		if d[0]=='.':
			print colored_text(d,toesc(90)),
			#print d, 
			#strform = '{:>'+str(maxlen)+'}'
			#print str(len(os.listdir(d))).format(strform)
			print ' '*(maxlen-len(str(d))),
			print colored_text(str(len(os.listdir(d)))+'   items',toesc(90))
		else:
			print colored_text(d,toesc(94)),
			#print d, 
			#strform = '{:>'+str(maxlen)+'}'
			#print str(len(os.listdir(d))).format(strform)
			print ' '*(maxlen-len(str(d))),
			print colored_text(str(len(os.listdir(d)))+'   items',toesc(94))


def printFiles(files,maxlen):
	dim=['b','k', 'mb','gb']
	for f in files:
		size = os.path.getsize(f)
		dimIndex = 0
		while (size>1000):
			dimIndex +=1
			size = size/1000
		if f[0]=='.':
			print colored_text(f,toesc(90)),
			print ' '*(maxlen-len(str(f))),
			
			print colored_text(str(size)+str(dim[dimIndex],toesc(90)))
		elif f[-3:]=='.py':			
			print colored_text(f,toesc(92)),
			print ' '*(maxlen-len(str(f))),
			print colored_text(f[f.rfind('.'):],toesc(92)),
			print colored_text(str(size)+str(dim[dimIndex]),toesc(92))
		else:
			print colored_text(f,toesc(37)),					
			print ' '*(maxlen-len(str(f))),
			print colored_text(f[f.rfind('.'):],toesc(37)),	
			print colored_text(str(size)+str(dim[dimIndex]),toesc(37))

dirs=getDirs(sys.argv[1])
files= getFiles(sys.argv[1])
maxlen = maxDim(dirs + files)
printDirs(dirs,maxlen)
printFiles(files,maxlen)
