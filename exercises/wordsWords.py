def splits(word):
    """
    returns a list of all posible splits of word
    include the empty splits
    returns a list of tuples.
    #>>> splits('ana')
    [('', 'ana'), ('a', 'na'), ('an', 'a'), ('ana', '')]
    """
    listRes=[]
    [listRes.append((word[0:index],word[index:])) for index in xrange(len(word)+1)]
    return listRes

def deletes(word):
    """
    all misspellings of word caused by omitting a letter
    use the splits function
    #>>> 'rockets' not in deletes('rocket')
    True
    #>>> 'roket' in deletes('rocket')
    True
    """
    listSplits=splits(word)
    return [b for a,b in listSplits]

def substitute(word):
    chrs=[chr(i) for i in xrange(ord('a'),ord('z')+1)]
    splitsList = splits(word)
    resList=[]
    for sp in splitsList:
        for c in chrs:
            if sp[0] != '':
                subStr=sp[0][:-1]+c
                resList.append(subStr+sp[1])
    return resList

def testSplits():
    assert splits('ana') ==[('', 'ana'), ('a', 'na'), ('an', 'a'), ('ana', '')]

def testDeletes():
    assert 'rockets' not in deletes('rocket')
    assert 'rocket' in deletes('rocket')


def testSubstitute():
    assert 'apa' in substitute('ana')
